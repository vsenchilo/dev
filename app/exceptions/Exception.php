<?php

namespace Exceptions;

class Exception extends \Exception {
    protected $errors = [];

    public function __construct(string $message)
    {
        parent::__construct($message);
    }

    public function setErrors(array $errors) : self
    {
        $this->errors = $errors;

        return $this;
    }

    public function getErrors() : array
    {
        return $this->errors;
    }
}