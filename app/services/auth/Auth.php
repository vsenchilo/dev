<?php

declare (strict_types = 1);

namespace Services\Auth;

use Core\Controllers\BaseController;
use Phalcon\Http\Response\Cookies;
use Phalcon\Http\Request;
use App\Collections\ {
    User,
    Session,
    ConfirmCode
};
use Exceptions\API\AuthException;
use Exceptions\API\ConfirmException;
use Exceptions\Exception;
use Exceptions\SessionException;
use Services\Auth\Storages\iStorage;

class Auth
{
    private $storageProvider;
    private $session;

    public function setStorageProvider(iStorage $provider)
    {
        $this->storageProvider = $provider;
    }

    public function getSessionByHash(string $hash) : ?Session
    {
        $interfaces = class_implements($this->storageProvider);
        if (is_array($interfaces) && isset($interfaces[iStorage::class])) {
            $this->session = $this->storageProvider->getSessionByString(
                $this->storageProvider->requestAuthString()
            );
        }

        return $this->session;
    }

    public function requestSignin(string $email, string $password) : self
    {
        $user = User::getRegistred($email, $password);

        if ($user instanceof User) {
            $this->session = $this->signin($user);
        }

        return $this;
    }

    public function signin(User $user) : Session
    {
        $session = new Session();
        $session->setUser($user);

        $interfaces = class_implements($this->storageProvider);
        if (is_array($interfaces) && isset($interfaces[iStorage::class])) {
            $this->storageProvider->setSession($session);

            // ToDo: обрабарывать статус, если сессия не сохранилась
            $status = $this->storageProvider->saveSession();
        }

        return $session;
    }

    public function getSession() : ?Session
    {
        return $this->session;
    }

    public function assignSession(BaseController $controller)
    {
        // $controller->userSession
    }

    public static function createToken(int $length) : string
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}
