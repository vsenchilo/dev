<?php

declare (strict_types = 1);

namespace Services\Auth\Storages;

use Phalcon\Mvc;
use App\Collections\Session;
use Services\Auth\Auth;
use App\Collections\User;

class Cookies extends Mvc\Controller implements iStorage
{
    const TOKEN_PATTERN = '%s#%s#%s#%s';
    const STORAGE_TYPE = 'cookies';
    const STORAGE_NAME = 'ahash';

    private $sessionTTL = 86400;
    private $session;

    public function getStorageName() : string
    {
        return self::STORAGE_NAME;
    }

    public function setSession(Session $session) : iStorage
    {
        $this->session = $session;

        return $this;
    }

    public function saveSession() : bool
    {
        $status = false;

        if ($this->session instanceof Session) {
            $this->session->setToken(
                Auth::createToken(32)
            );
            $this->session->setAuthHash(
                $this->createAuthHash()
            );
            $this->session->setStorageType(
                self::STORAGE_TYPE
            );
            $this->session->setSessionTTL(
                $this->sessionTTL
            );

            $this->session->user_ip = $this->request->getClientAddress(true);
            $this->session->user_client = $this->request->getUserAgent();

            $status = $this->session->save();

            if ($status) {
                $this->cookies->set(
                    self::STORAGE_NAME,
                    $this->session->getAuthHash(),
                    time() + $this->sessionTTL
                );
            }
        }

        return $status;
    }

    public function getSessionByString(?string $hash) : ?Session
    {
        $session = Session::findFirst(
            [
                'conditions' => [
                    'storage_type' => self::STORAGE_TYPE,
                    'auth_hash' => $hash
                ],
                'sort' => [
                    '_id' => -1
                ]
            ]
        );

        if ($session instanceof Session) {
            $session->setUser(
                User::findById($session->getUserId())
            );

            $this->session = $session;
        }

        return $this->session;
    }

    private function createAuthHash() : ?string
    {
        $ahash = null;

        if ($this->session instanceof Session) {
            $ahash = md5(sprintf(
                self::TOKEN_PATTERN,
                $this->session->getToken(),
                $this->request->getClientAddress(true),
                $this->request->getUserAgent(),
                (string)$this->session->getUser()->getId()
            ));
        }

        return $ahash;
    }

    public function checkAuthString() : bool
    {
        if (!($this->session instanceof Session)) {
            return false;
        }

        $ahash = md5(sprintf(
            self::TOKEN_PATTERN,
            $this->session->getToken(),
            $this->request->getClientAddress(true),
            $this->request->getUserAgent(),
            (string)$this->session->getUser()->getId()
        ));

        return $ahash === $this->requestAuthString();
    }

    public function requestAuthString() : ?string
    {
        $string = null;

        if ($this->cookies->has(self::STORAGE_NAME)) {
            $string = $this->cookies->get(self::STORAGE_NAME)->getValue();
        }

        return $string;
    }

    public function getAuthString() : ? string
    {
        return $this->session->getAuthHash();
    }
}
