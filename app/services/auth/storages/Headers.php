<?php

declare (strict_types = 1);

namespace Services\Auth\Storages;

use Phalcon\Mvc;
use App\Collections\Session;
use Services\Auth\Auth;
use App\Collections\User;

class Headers extends Mvc\Controller implements iStorage
{
    const TOKEN_PATTERN = '%s#%s#%s#%s';
    const STORAGE_TYPE = 'headers';

    const STORAGE_NAME = 'x-auth-header';

    private $sessionTTL = 86400;
    private $session;

    public function getStorageName() : string
    {
        return self::STORAGE_NAME;
    }

    public function setSession(Session $session): iStorage
    {
        $this->session = $session;

        return $this;
    }

    public function saveSession() : bool
    {
        $status = false;

        if ($this->session instanceof Session) {
            $this->session->setToken(
                Auth::createToken(32)
            );
            $this->session->setAuthHash(
                $this->createAuthHash()
            );
            $this->session->setStorageType(
                self::STORAGE_TYPE
            );
            $this->session->setSessionTTL(
                $this->sessionTTL
            );

            $this->session->user_ip = $this->request->getClientAddress(true);
            $this->session->user_client = $this->request->getUserAgent();

            $status = $this->session->save();
        }

        return $status;
    }

    public function getSessionByString(?string $token) : ?Session
    {
        $session = Session::findFirst(
            [
                'conditions' => [
                    'storage_type' => self::STORAGE_TYPE,
                    'token' => $token
                ],
                'sort' => [
                    '_id' => -1
                ]
            ]
        );

        if ($session instanceof Session) {
            $session->setUser(
                User::findById($session->getUserId())
            );

            $this->session = $session;
        }

        return $this->session;
    }

    private function createAuthHash() : ?string
    {
        $ahash = null;

        if ($this->session instanceof Session) {
            $ahash = md5(sprintf(
                self::TOKEN_PATTERN,
                $this->session->getToken(),
                $this->request->getClientAddress(true),
                $this->request->getUserAgent(),
                (string)$this->session->getUser()->getId()
            ));
        }

        return $ahash;
    }

    public function checkAuthString() : bool
    {
        return $this->session instanceof Session;
    }

    public function requestAuthString() : ?string
    {
        return $this->request->getHeader(self::STORAGE_NAME);
    }

    public function getAuthString() : ?string
    {
        return $this->session->getToken();
    }

    public function createToken(int $length) : string
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}
