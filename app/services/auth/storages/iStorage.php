<?php

declare (strict_types = 1);

namespace Services\Auth\Storages;

use App\Collections\Session;

interface iStorage
{
    // Assign session for storage
    public function setSession(Session $session) : iStorage;

    // Save/update session in storage
    public function saveSession() : bool;

    // Find session from recieved string
    public function getSessionByString(?string $string) : ?Session;

    // Return current storage name
    public function getStorageName() : string;

    // String from request cookies/headers etc.
    public function requestAuthString() : ?string;

    // String from current session
    public function getAuthString() : ?string;

    // Check string from request for valid
    public function checkAuthString() : bool;
}