<?php

declare (strict_types = 1);

namespace Services\Messangers;

interface iMessanger
{
    public function send(string $message) : bool;
    public function getErrors() : array;
    public function setParam(string $key, string $val) : self;
    public function getSubscribers() : array;
}
