<?php

declare (strict_types = 1);

namespace Services\Messangers\Mailer;

class Subscribe extends Mailer
{
    private $_user;

    public function send() : bool
    {
        $to = $this->_params['email'];

        $message = $this->_mailer->createMessageFromView('subscribe.volt', $this->_params)
            ->to($to, 'in4line')
            ->subject('in4line  Подписка на канал');

        return (bool)$message->send();
    }
}