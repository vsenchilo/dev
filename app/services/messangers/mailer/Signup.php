<?php

declare (strict_types = 1);

namespace Services\Messangers\Mailer;

class Signup extends Mailer
{
    private $_user;

    public function send() : bool
    {
        $message = $this->_mailer->createMessageFromView('signup.volt', $this->_params)
            ->to($this->_params['email'], 'in4line')
            ->subject('in4line Регистрация');

        return (bool)$message->send();
    }
}