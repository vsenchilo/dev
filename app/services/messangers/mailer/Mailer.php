<?php

declare (strict_types = 1);

namespace Services\Messangers\Mailer;

use Services\Messangers\iMessanger;
use App\Collections\User;

abstract class Mailer implements iMessanger
{
    private $password = 'bGV2ZnE5NTgwMTk=';
    private $login = 'senchilovictor@gmail.com';

    protected $_config;
    protected $_mailer;
    protected $_view;
    protected $_params = [];

    public function __construct()
    {
        $this->_config = [
            'driver' => 'smtp',
            'host' => 'smtp.gmail.com',
            'port' => 465,
            'encryption' => 'ssl',
            'username' => $this->login,
            'password' => base64_decode($this->password),
            'viewsDir' => __DIR__ . '/views/',
            'from' => [
                'email' => 'in4line@gmail.com',
                'name' => 'Info Line'
            ]
        ];

        $this->_mailer = new \Phalcon\Ext\Mailer\Manager($this->_config);
    }

    abstract function send() : bool;

    public function setParam(string $key, string $val) : iMessanger
    {
        $this->_params[$key] = $val;

        return $this;
    }

    public function setView(string $view) : self
    {
        $this->_view = $view;

        return $this;
    }

    public function getErrors() : array
    {
        return [];
    }
}