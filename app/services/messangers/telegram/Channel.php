<?php

declare (strict_types = 1);

namespace Services\Messangers\Telegram;

class Channel extends Telegram
{
    private $subscribers = [];

    public function send(string $message): bool
    {
        $this->subscribers = $this->getSubscribers();

        foreach ($this->subscribers as $telegramToken => $subscribers) {
            foreach ($subscribers as $userId => $chatId) {
                $this->getTelegramBot($telegramToken)->sendMessage([
                    'chat_id' => $chatId,
                    'text' => $message,
                    'parse_mode' => 'html'
                ]);
            }
        }

        return true;
    }
}
