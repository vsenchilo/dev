<?php

declare (strict_types = 1);

namespace Services\Messangers\Telegram;

use Services\Messangers\iMessanger;
use App\Collections\Channel as ChannelCollection;
use App\Collections\Telegram as TelegramCollection;

abstract class Telegram implements iMessanger
{
    private $channel;
    private $telegram;
    private $telegramBots = [];

    public function __construct(ChannelCollection $channel, TelegramCollection $telegram)
    {
        $this->channel = $channel;
        $this->telegram = $telegram;

        foreach ($this->getTelegramCollection()->getTokens() as $token) {
            $this->telegramBots[$token] = new \Telegram\Bot\Api($token);
        }

    }

    public function getErrors(): array
    {
        return [];
    }

    public function setParam(string $key, string $val): iMessanger
    {
        return $this;
    }

    protected function getChannelCollection(): ChannelCollection
    {
        return $this->channel;
    }

    protected function getTelegramCollection(): TelegramCollection
    {
        return $this->telegram;
    }

    protected function getTelegramBot(string $token): \Telegram\Bot\Api
    {
        return $this->telegramBots[$token];
    }

    public function getSubscribers(): array
    {
        return $this->getTelegramCollection()->getSubscribers();
    }
}
