<?php

declare (strict_types = 1);

namespace Services\Generators\String;

use Services\Generators\iGenerator;

class Generator implements iGenerator
{
    private $strLength;
    private $string;

    public function __construct($strLength = 6)
    {
        $this->strLength = $strLength;
    }

    public function generate() : string
    {
        $this->code = '';

        $vowels = 'aeiou';
        $consonants = 'bcdfghjklmnprstvwxyz';

        $max = $this->strLength / 2;

        for ($i = 1; $i <= $max; $i++) {
            $this->code .= $consonants[rand(0, 19)];
            $this->code .= $vowels[rand(0, 4)];
        }

        $this->code = strtoupper($this->code);

        $this->code .= '-' . rand(10, 99);

        return $this->code;
    }
}
