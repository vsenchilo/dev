<?php

declare (strict_types = 1);

namespace Services\Generators;

interface iGenerator
{
    public function generate() : string;
}