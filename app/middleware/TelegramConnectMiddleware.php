<?php

declare (strict_types = 1);

namespace App\Middleware;

use Phalcon\Mvc\Dispatcher;
use Core\Controllers\BaseController as Controller;
use App\Collections\Channel;

class TelegramConnectMiddleware implements iMiddleware
{
    private $status = true;
    private $interrupt = true;

    public function call(Dispatcher $dispatcher, Controller $controller) : iMiddleware
    {
        $channelId = $controller->request->getPost('channel');
        $token = $controller->request->getPost('token');

        $channel = Channel::findFirst([
            'conditions' => [
                '_id' => new \MongoDB\BSON\ObjectID($channelId),
                'owner_id' => $controller->getUserSession()->getUserId()
            ]
        ]);

        if (!$channel) {
            $dispatcher->forward([
                'namespace' => 'App\Http\Api\Controllers',
                'controller' => 'error',
                'action' => 'error400',
            ]);
        }

        return $this;
    }

    public function hasFail(): bool
    {
        if (false === $this->interrupt) {
            return false;
        }

        return false === $this->status;
    }
}
