<?php

declare (strict_types = 1);

namespace App\Middleware;

use Phalcon\Mvc\Dispatcher;
use Core\Controllers\BaseController as Controller;
use Services\Auth\Auth;

class AuthMiddleware implements iMiddleware
{
    private $status = true;
    private $storage;
    private $redirect = true;
    private $interrupt = true;

    public function call(Dispatcher $dispatcher, Controller $controller) : iMiddleware
    {
        if (null === $this->storage) {
            $this->storage = sprintf(
                'Services\\Auth\\Storages\\%s',
                ucfirst($dispatcher->getParam('storage'))
            );
        }

        $auth = new Auth();
        $this->storage = new $this->storage;

        $auth->setStorageProvider($this->storage);
        $auth->getSessionByHash(
            $controller->request->getHeader($this->storage->getStorageName())
        );

        if (!$this->storage->checkAuthString()) {
            $this->status = false;

            if (true === $this->redirect) {
                $dispatcher->forward([
                    'namespace' => 'App\Http\Api\Controllers',
                    'controller' => 'error',
                    'action' => 'error401'
                ]);
            }
        } else {
            $controller->setUserSession(
                $auth->getSession()
            );
        }

        return $this;
    }

    public function hasFail() : bool
    {
        if (false === $this->interrupt) {
            return false;
        }

        return false === $this->status;
    }

    public function setStorage(string $storage) : self
    {
        $this->storage = $storage;

        return $this;
    }

    public function redirect(bool $redirect) : self
    {
        $this->redirect = $redirect;

        return $this;
    }

    public function interrupt(bool $interrupt) : self
    {
        $this->interrupt = $interrupt;

        return $this;
    }
}