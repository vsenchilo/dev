<?php

declare (strict_types = 1);

namespace App\Middleware;

use Phalcon\Mvc\Dispatcher;
use Core\Controllers\BaseController as Controller;

interface iMiddleware
{
    public function call(Dispatcher $dispatcher, Controller $controller) : self;
    public function hasFail() : bool;
}
