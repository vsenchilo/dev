<?php

declare(strict_types=1);

date_default_timezone_set('UTC');

use Phalcon\Cache\{
    Frontend\Data as FrontData,
    Backend\Libmemcached as Memcache
};
use Phalcon\DI\FactoryDefault;
use Phalcon\Loader;
use Phalcon\Mvc\{
    Application,
    Collection\Manager as CollectionManager,
    Dispatcher,
    Router,
    View,
    View\Engine\Volt
};
use Phalcon\Db\Adapter\MongoDB\Client as MongoClient;
use Phalcon\Db\Adapter\Pdo\Mysql as MySQLCLient;
use Phalcon\Http\Response\Cookies;

$loader = new Loader();
$di = new FactoryDefault;

$di->setShared('di', $di);

$loader->registerNamespaces([
    'Exceptions' => APP_PATH . '/exceptions',
    'API\Exceptions' => APP_PATH . '/exceptions/API',

    'Services\Auth' => APP_PATH . '/services/auth',
    'Services\Auth\Storages' => APP_PATH . '/services/auth/storages',

    'Services\Generators' => APP_PATH . '/services/generators',
    'Services\Generators\String' => APP_PATH . '/services/generators/string',

    'Services\Messangers' => APP_PATH . '/services/messangers',
    'Services\Messangers\Mailer' => APP_PATH . '/services/messangers/mailer',
    'Services\Messangers\Telegram' => APP_PATH . '/services/messangers/telegram',

    'Core\Interfaces\Http' => CORE_PATH . '/interfaces/http',
    'Core\Traits\Http' => CORE_PATH . '/traits/http',
    'Core\Controllers' => CORE_PATH . '/controllers',
    'App\Exceptions' => APP_PATH . '/exceptions',

    'App\Http\Api\Core\Controllers' => APP_PATH . '/http/api/core/controllers',
    'App\Http\Api\Controllers' => APP_PATH . '/http/api/controllers',

    'App\Http\Frontend\Core\Controllers' => APP_PATH . '/http/frontend/core/controllers',
    'App\Http\Frontend\Controllers' => APP_PATH . '/http/frontend/controllers',

    'App\Http\Backend\Core\Controllers' => APP_PATH . '/http/backend/core/controllers',
    'App\Http\Backend\Controllers' => APP_PATH . '/http/backend/controllers',

    'App\Middleware' => APP_PATH . '/middleware',
    'App\Collections' => APP_PATH . '/collections',
    'App\Collections\Validators' => APP_PATH . '/collections/validators',
    'App\Models' => APP_PATH . '/models',

    'App\Http\Api\Requests' => APP_PATH . '/http/api/requests'
])->register();

$di->set(
    'collectionManager',
    function () {
        return new CollectionManager();
    }
);

$di->set(
    'modelManager',
    function () {
        return new \Phalcon\Mvc\Model\Manager();
    }
);

$di->set(
    'mongo',
    function () use ($config) {
        $client = new MongoClient(
            sprintf('mongodb://%s:%s/%s', $config->mongo->host, $config->mongo->port, $config->mongo->dbname),
            ['username' => $config->mongo->user, 'password' => $config->mongo->password]
        );

        return $client->selectDatabase($config->mongo->dbname);
    }
);

$di->set(
    'db',
    function () use ($config) {
        return new MySQLCLient(
            [
                'host' => $config->mysql->host,
                'dbname' => $config->mysql->dbname,
                'port' => $config->mysql->port,
                'username' => $config->mysql->user,
                'password' => $config->mysql->password,
                'adapter' => $config->mysql->adapter
            ]
        );
    }
);

$di->set(
    'uploadDir',
    function () : string {
        return UPLOADED_PATH;
    }
);

$di->set(
    'cookies',
    function () {
        $cookies = new Cookies();

        $cookies->useEncryption(false);

        return $cookies;
    }
);

$di->set(
    'memcache',
    function () : Memcache {
        return new Memcache(
            new FrontData([
                'lifetime' => 3600
            ]),
            [
                # Note: statsKey has a negative performance impact
                # 'statsKey' => '_PHCM',
                'server' => [
                    'host'       => 'localhost',
                    'port'       => 11211,
                    'persistent' => false
                ]
            ]
        );
    }
);

$di->set(
    'router',
    function () use ($di) : Router {
        $router = new Router(false);

        require_once ROUTER_PATH . '/api.php';
        require_once ROUTER_PATH . '/backend.php';
        require_once ROUTER_PATH . '/frontend.php';

        $router->removeExtraSlashes(true);

        return $router;
});

$di->set(
    'dispatcher',
    function () : Dispatcher {
        $dispatcher = new Dispatcher();
        $eventsManager = new Phalcon\Events\Manager();

        $eventsManager->attach(
            'dispatch:beforeException',
            function ($event, $dispatcher, $exception) {
                switch ($exception->getCode()) {
                    case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward(
                            [
                                'namespace' => 'App\Http\Api\Controllers',
                                'controller' => 'error',
                                'action' => 'error404'
                            ]
                        );

                        return false;
                }
            }
        );

        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
}, true);

$di->set(
    'voltService',
    function ($view, $di) : Volt {
        $volt = new Volt(
            $view,
            $di
        );

        $volt->setOptions([
            'compiledPath' => BASE_PATH . '/storage/cache/views/',
            'compiledExtension' => '.compiled',
            'compiledSeparator' => '_',
            'compileAlways' => true,
            'stat' => true
        ]);

        return $volt;
    }
);

$di->set('view', function () : View {
    $view = new View;

    $view->setViewsDir(BASE_PATH . '/views/');
    $view->registerEngines([
        '.volt'  => 'voltService',
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ]);

    return $view;
});

(new Application($di))
    ->handle()
    ->send();
