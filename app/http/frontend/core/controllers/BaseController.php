<?php

declare (strict_types = 1);

namespace App\Http\Frontend\Core\Controllers;

use App\Collections\Session;
use Services\Auth\Storages\Cookies;
use Phalcon\Mvc\Dispatcher;
use Services\Auth\Auth;

abstract class BaseController extends \Core\Controllers\BaseController
{
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $action = $dispatcher->getActionName();

        if (!empty($this->getMiddlewares())) {
            foreach ($this->getMiddlewares() as $middleware) {
                if (in_array($action, $middleware['action'])) {
                    if ($middleware['middleware']->call($dispatcher, $this)->hasFail()) {
                        break;
                    }
                }
            }
        }
    }

    public function initialize()
    {
        $this->view->setViewsDir(VIEW_DIR . '/frontend');

        if ($this->getUserSession() instanceof Session) {
            $this->view->user = $this->getUserSession()->getUser();
        }
    }
}
