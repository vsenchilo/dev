<?php

declare (strict_types = 1);

namespace App\Http\Frontend\Controllers;

use App\Http\Frontend\Core\Controllers\BaseController;
use App\Collections\User;
use App\Middleware\AuthMiddleware;
use Phalcon\Mvc\Dispatcher;
use Services\Auth\Storages\Cookies;
use App\Collections\Channel;
use App\Collections\Telegram;

class IndexController extends BaseController
{
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->addMiddleware(
            (new AuthMiddleware())
                ->setStorage(Cookies::class)
                ->interrupt(false)
                ->redirect(false),
            ['index', 'technical']
        );

        parent::beforeExecuteRoute($dispatcher);
    }

    public function indexAction()
    {
        $channels = Channel::find();

        $this->view->channels = $channels;
    }

    public function technicalAction()
    {
    }

    public function usersAction()
    {
        $user = User::find();
        echo '<pre>';
        print_r($user);
        exit;
    }
}
