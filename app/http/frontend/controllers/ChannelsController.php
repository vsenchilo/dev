<?php

declare (strict_types = 1);

namespace App\Http\Frontend\Controllers;

use App\Middleware\AuthMiddleware;
use App\Http\Frontend\Core\Controllers\BaseController;
use Phalcon\Mvc\Dispatcher;
use Services\Auth\Storages\Cookies;
use App\Collections\Channel;
use App\Collections\Telegram;

class ChannelsController extends BaseController
{
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->addMiddleware(
            (new AuthMiddleware())
                ->setStorage(Cookies::class),
            ['subscriptions', 'list', 'create', 'edit', 'telegram', 'channel']
        );

        parent::beforeExecuteRoute($dispatcher);
    }

    public function listAction()
    {
        $user = $this->getUserSession()->getUser();

        $this->view->channels = $user->getChannels();
    }

    public function channelAction()
    {
        $this->view->channe_id = $this->dispatcher->getParam('id');
    }

    public function createAction()
    {
    }

    public function subscriptionsAction()
    {
        $user = $this->getUserSession()->getUser();

        $this->view->subscriptions = $user->getSubscriptions();
    }

    public function editAction()
    {
        $channel = Channel::findFirst([
            'conditions' => [
                'owner_id' => $this->getUserSession()->getUserId(),
                '_id' => new \MongoDB\BSON\ObjectID($this->dispatcher->getParam('id'))
            ]
        ]);

        if (!$channel) {
            return $this->dispatcher->forward([
                'namespace' => 'App\Http\Api\Controllers',
                'controller' => 'error',
                'action' => 'error400',
            ]);
        }

        $this->view->channel = $channel;
    }

    public function telegramAction()
    {
        $channel = Channel::findById($this->dispatcher->getParam('id'));

        if (!$channel) {
            return $this->dispatcher->forward([
                'namespace' => 'App\Http\Api\Controllers',
                'controller' => 'error',
                'action' => 'error400',
            ]);
        }

        $telegram = Telegram::findFirst(['conditions' => [
            'owner_id' => $this->getUserSession()->getUserId(),
            'channel_id' => $channel->getId()
        ]]);

        $this->view->channel = $channel;
        $this->view->telegram = $telegram;
    }
}
