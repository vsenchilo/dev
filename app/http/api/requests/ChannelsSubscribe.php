<?php

declare (strict_types = 1);

namespace App\Http\Api\Requests;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Regex;

class ChannelsSubscribe extends Validation
{
    public function initialize()
    {
        $this->add(
            'channel',
            new Regex([
                "pattern" => "/^[a-z0-9]{24}$/",
                "message" => "Invalid channel id",
            ])
        );
    }

    public function getRequest() : array
    {
        return $this->request->getPost();
    }
}
