<?php

declare (strict_types = 1);

namespace App\Http\Api\Requests;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class TelegramConnect extends Validation
{
    public function initialize()
    {
        $this->add(
            ['channel', 'token'],
            new PresenceOf([
                'message' => 'поле не может быть пустым',
                'message' => 'поле не может быть пустым'
            ])
        );
    }

    public function getRequest() : array
    {
        return $this->request->getPost();
    }
}
