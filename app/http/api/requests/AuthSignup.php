<?php

declare (strict_types = 1);

namespace App\Http\Api\Requests;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Uniqueness;
use App\Collections\User;

class AuthSignup extends Validation
{
    public function initialize()
    {
        $this->add(
            'email',
            new PresenceOf([
                'message' => 'необходимо указать email',
            ])
        );

        $this->add(
            'email',
            new Email(
                [
                    'message' => 'email не валидный',
                ]
            )
        );

        $this->add(
            'password',
            new PresenceOf([
                'message' => 'необходимо указать password',
            ])
        );
    }

    public function getRequest() : array
    {
        return $this->request->getPost();
    }
}
