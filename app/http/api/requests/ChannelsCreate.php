<?php

declare (strict_types = 1);

namespace App\Http\Api\Requests;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\PresenceOf;
use App\Collections\Channel;

class ChannelsCreate extends Validation
{
    public function initialize()
    {
        $this->add(
            'title',
            new PresenceOf([
                'message' => 'поле не может быть пустым',
            ])
        );

        $this->add(
            'alias',
            new PresenceOf([
                'message' => 'поле не может быть пустым',
            ])
        );

        $this->add(
            'alias',
            new Regex([
                'pattern' => '/^[0-9a-z-_]{2,32}$/',
                'message' => 'должно содержать от 2 до 8 символов и может содержать символы 0-9a-z-_'
            ])
        );

        $this->add(
            'alias',
            new Uniqueness([
                'model' => new Channel,
                'message' => 'должно быть уникальным',
            ])
        );
    }

    public function getRequest() : array
    {
        return $this->request->getPost();
    }
}
