<?php

declare (strict_types = 1);

namespace App\Http\Api\Core\Controllers;

use App\Collections\User;
use Phalcon\Http\Response;
use Phalcon\Mvc\Dispatcher;
use Services\Auth\FactoryAuthHeaders;
use Services\Validators\ValidatorFactory;
use Services\Auth\Auth;
use Services\Auth\Storages\Headers;
use Services\Auth\Storages\Cookies;
use App\Middleware\iMiddleware;

abstract class BaseController extends \Core\Controllers\BaseController
{
    protected $requestRules = [];

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $action = $dispatcher->getActionName();
        $controller = $dispatcher->getControllerName();

        if (!empty($this->getMiddlewares())) {
            foreach ($this->getMiddlewares() as $middleware) {
                if (in_array($action, $middleware['action'])) {
                    $mid = new $middleware['middleware'];

                    if ($mid->call($dispatcher, $this)->hasFail()) {
                        break;
                    }
                }
            }
        }

        if (false === $dispatcher->wasForwarded()) {
            $validator = sprintf("App\\Http\\Api\\Requests\\%s%s",
                ucfirst($controller),
                ucfirst($action)
            );

            if (class_exists($validator)) {
                $validator = new $validator;

                $messages = $validator->validate(
                    $validator->getRequest()
                );

                if (count($messages)) {
                    $_messages = [];
                    foreach ($messages as $message) {
                        $_messages[$message->getField()][] = $message->getMessage();
                    }

                    $dispatcher->forward([
                        'namespace' => 'App\Http\Api\Controllers',
                        'controller' => 'error',
                        'action' => 'error400',
                        'params' => ['errors' => $_messages]
                    ]);
                }
            }
        }
    }

    public function initialize()
    {
        $this->view->disable();
    }

    public function prepareErrorsMessages($messages) : array
    {
        $errors = [];

        foreach ($messages as $message) {

        }

        return $errors;
    }

    public function respondError(array $content = [], int $status = 500) : Response
    {
        return $this->respond($content, $status);
    }

    public function respondSuccess(array $content = [], int $status = 200) : Response
    {
        return $this->respond($content, $status);
    }

    public function respond(array $content, int $status) : Response
    {
        $content = json_encode($content);

        $this->response->setContentLength(strlen($content));
        $this->response->setStatusCode($status, self::$statusCodes[$status]);
        $this->response->setContentType('application/json', 'UTF-8');
        $this->response->setContent($content);
        $this->response->sendHeaders();

        return $this->response;
    }
}
