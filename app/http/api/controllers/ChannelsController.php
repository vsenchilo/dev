<?php

declare (strict_types = 1);

namespace App\Http\Api\Controllers;

use Phalcon\Http\{
    Response,
    Request
};
use App\Http\Api\Core\Controllers\BaseController;
use App\Collections\Channel;
use App\Exceptions\ChannelException;
use Phalcon\Mvc\Dispatcher;
use App\Middleware\AuthMiddleware;
use App\Exceptions\UserException;
use App\Collections\Telegram;
use Services\Messangers\Telegram\Channel as ServicesChannel;

class ChannelsController extends BaseController
{
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->addMiddleware(
            new AuthMiddleware,
            ['create', 'subscribe', 'sendmessage', 'list']
        );

        parent::beforeExecuteRoute($dispatcher);
    }

    public function createAction() : Response
    {
        $user = $this->getUserSession()->getUser();

        $channel = new Channel();
        $channel->setTitle(
            $this->request->getPost('title')
        );

        if ($this->request->getPost('alias')) {
            $channel->setAlias(
                $this->request->getPost('alias')
            );
        }

        if ($this->request->getPost('description')) {
            $channel->setDescription(
                $this->request->getPost('description')
            );
        }

        if ($this->request->getPost('metaDescription')) {
            $channel->setMetaDescription(
                $this->request->getPost('metaDescription')
            );
        }

        if ($this->request->getPost('metaKeywords')) {
            $channel->setMetaKeywords(
                $this->request->getPost('metaKeywords')
            );
        }

        $channel->setOwner($user);

        try {
            $user->addChannel($channel);
        } catch (ChannelException $e) {
            return $this->respondError([
                'message' => $e->getMessage(),
                'errors' => $channel->getErrors()
            ], 400);
        }

        return $this->respondSuccess(['channel_id' => (string)$channel->getId()]);
    }

    public function telegramlistAsction() : Response
    {
        echo 'asd';exit;
        return $this->respondSuccess([
        ]);
    }

    public function listAction() : Response
    {
        $_channels = $this->getUserSession()->getUser()->getChannels();
        $channels = [];

        foreach ($_channels as $channel) {
            $channels[] = [
                '_id' => (string)$channel->getId(),
                'title' => $channel->getTitle(),
                'description' => $channel->getDescription()
            ];
        }

        return $this->respondSuccess([
            'channels' => $channels
        ]);
    }

    public function subscribeAction() : Response
    {
        $session = $this->getUserSession();

        if (null === $session) {
            return $this->respondError([], 401);
        }

        $channel = Channel::findById(
            $this->request->getPost('channel')
        );

        if (false === $channel) {
            return $this->respondError([], 400);
        }

        // $mailer = new Mailer\Subscribe();

        try {
            $channel->subscribe(
                $this->getUserSession()->getUser()
            );
        } catch (ChannelException $e) {
            return $this->respondError(['message' => $e->getMessage()]);
        } catch (UserException $e) {
            return $this->respondError(['message' => $e->getMessage()]);
        }

        // $channel
        //         ->setMessanger($mailer)
        //         ->sendSubscribeMessage();

        $channel->save();

        return $this->respondSuccess();
    }

    public function sendmessageAction()
    {
        $channelCollection = Channel::findFirst([
            'conditions' => [
                'owner_id' => $this->getUserSession()->getUserId(),
                '_id' => new \MongoDB\BSON\ObjectID($this->dispatcher->getParam('id'))
            ]
        ]);

        if (!$channelCollection) {
            return $this->dispatcher->forward([
                'namespace' => 'App\Http\Api\Controllers',
                'controller' => 'error',
                'action' => 'error400',
            ]);
        }

        $telegramCollection = Telegram::findFirst(['conditions' => ['channel_id' => $channelCollection->getId()]]);

        if ($telegramCollection) {
            $telegramMessanger = new ServicesChannel($channelCollection, $telegramCollection);
            $telegramMessanger->send($this->request->getPost('message'));
        }

        return $this->respondSuccess();
    }

    // public function editChannel() : Response
    // {

    // }

    public function testAction() : Response
    {
        return $this->respondSuccess();
    }
}