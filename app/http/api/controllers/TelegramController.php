<?php

declare (strict_types = 1);

namespace App\Http\Api\Controllers;

use App\Http\Api\Core\Controllers\BaseController;
use App\Middleware\AuthMiddleware;
use App\Middleware\TelegramConnectMiddleware;
use App\Collections\Channel;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Http\Client\Request;
use App\Collections\Telegram;
use App\Collections\User;
use Exceptions\API\TelegramException;

class TelegramController extends BaseController
{
    private $telegram;
    private $channelToken;
    private $botUpdates;
    private $botAction;
    private $botText;

    const BOT_COMMAND_START = '/start';
    const BOT_COMMAND_SET_EMAIL = '/email';
    const BOT_COMMANT_CONFIRM_EMAIL = '/econfirm';
    const TELEGRAM_API_BASE_URI = 'https://api.telegram.org/';
    const TELEGRAM_API_WEB_HOOK = 'bot%s/setWebhook?url=https://in4line.com/api/v1/telegram/webhook/%s';

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->addMiddleware(
            new AuthMiddleware,
            ['connect']
        );

        parent::beforeExecuteRoute($dispatcher);
    }

    public function webhookAction()
    {
        $this->telegram = new \Telegram\Bot\Api(
            $this->dispatcher->getParam('channel')
        );
        $this->channelToken = $this->dispatcher->getParam('channel');

        $this->botUpdates = $this->telegram->getWebhookUpdates();

        if ($this->botUpdates['message']) {
            $this->botText = $this->botUpdates['message']['text'];
            preg_match('/^(?=\/).*?(?=\s|$)/', $this->botText, $action);
            $this->botAction = is_array($action) ? $action[0] : $action;

            if ($this->botAction) {
                $this->botText = mb_substr($this->botText, (strlen($this->botAction) + 1), null, 'utf-8');
            }
            file_put_contents(STORAGE_PATH . '/telegram', "\n\n", FILE_APPEND);

        } else {
            $this->dispatcher->forward([
                'namespace' => 'App\Http\Api\Controllers',
                'controller' => 'error',
                'action' => 'error400'
            ]);
        }

        if (self::BOT_COMMAND_START == $this->botAction) {
            $this->botStart();
        } elseif (self::BOT_COMMAND_SET_EMAIL == $this->botAction) {
            $this->botSetEmail();
        } else {
            $this->botUnknownCommand();
        }

        return $this->respondSuccess();
    }

    public function botUnknownCommand()
    {
        $this->telegram->sendMessage([
            'chat_id' => $this->getChatId(),
            'text' => '<b>Команда не распознана.</b>\nНапишите /help для получения списка доступных команд.',
            'parse_mode' => 'html'
        ]);
    }

    public function botStart()
    {
        $this->telegram->sendMessage([
            'chat_id' => $this->getChatId(),
            'text' => "Приветствуем на канале in4line! Прежде чем Вы сможете получать уведомления, Вам необходимо пройти идентификацию. Для индентификации следуйте простым иснтрукциям:"
        ]);

        $this->telegram->sendMessage([
            'chat_id' => $this->getChatId(),
            'text' => 'Укажите Ваш E-mail в формате\n<pre>/email johddoe@example.com</pre>',
            'parse_mode' => 'html'
        ]);
    }

    public function botSetEmail()
    {
        if (!filter_var($this->botText, FILTER_VALIDATE_EMAIL)) {
            $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => sprintf("Вы указали невалидный Email %s.Укажите Ваш E-mail в формате\n/email johddoe@example.com", $this->botText)
            ]);
        } else {
            $telegramChannel = Telegram::findFirst(['conditions' => [
                'tokens' => ['$in' => [$this->channelToken]]
            ]]);

            $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => 'test',
            ]);

            if (!$telegramChannel) {
                return $this->telegram->sendMessage([
                    'chat_id' => $this->getChatId(),
                    'text' => 'Канал не обнаружен',
                ]);
            }

            $subscriber = User::findFirst([
                'conditions' => ['email' => $this->botText]
            ]);

            if (!$subscriber) {
            //     $password = (new Generator(8))->generate();

            //     $subscriber = new User;
            //     $subscriber->setEmail($this->botText);
            //     $subscriber->setPassword(
            //         $password
            //     );
            //     $subscriber->save();
                return $this->telegram->sendMessage([
                    'chat_id' => $this->getChatId(),
                    'text' => 'Вы не зарегестрированы на сайте',
                ]);
            }

            // Суть проблемы - хранение подписчиков - нужно сделать унифицированное хранение подписчиков в коллекции либо в конкретном мессенджере, но если последнее - нужна верная линковка подписчиков на сайте, и тех, что в отдельно взятом мессенджере
            $telegramChannel->subscribe($subscriber, $this->channelToken, (string)$this->getChatId());
            $telegramChannel->save();

            return $this->telegram->sendMessage([
                'chat_id' => $this->getChatId(),
                'text' => sprintf("На указанный Вами E-mail (%s) отправлено сообщение в котором указан код для подтверждения. Введите код в формате\n/code XXXX-XX", $this->botText),
            ]);
        }
    }

    public function getChatId() : int
    {
        $chatId = -1;

        if (isset($this->botUpdates['message'])) {
            $chatId = $this->botUpdates['message']['from']['id'];
        }

        return $chatId;
    }

    public function connectAction()
    {
        $channelId = $this->request->getPost('channel');
        $token = $this->request->getPost('token');

        $channel = Channel::findFirst([
            'conditions' => [
                '_id' => new \MongoDB\BSON\ObjectID($channelId),
                'owner_id' => $this->getUserSession()->getUserId()
            ]
        ]);

        if (!$channel) {
            return $this->dispatcher->forward([
                'namespace' => 'App\Http\Api\Controllers',
                'controller' => 'error',
                'action' => 'error400',
            ]);
        }

        $telegram = Telegram::findFirst(['conditions' => [
            'owner_id' => $this->getUserSession()->getUserId()
        ]]);

        if (!$telegram) {
            $telegram = new Telegram;
        }

        try {
            $telegram
                ->setToken($this->request->getPost('token'))
                ->setChannel($channel)
                ->setOwner($this->getUserSession()->getUser());
            $telegram->save();
        } catch (TelegramException $e) {
            return $this->dispatcher->forward([
                'namespace' => 'App\Http\Api\Controllers',
                'controller' => 'error',
                'action' => 'error400',
            ]);
        }

        $webHook = sprintf(self::TELEGRAM_API_WEB_HOOK, $token, $token);
        $webHook = str_replace(':', '%3A', $webHook);
        $provider = Request::getProvider();
        $provider->setBaseUri(self::TELEGRAM_API_BASE_URI);
        $response = $provider->get($webHook);

        // var_dump( $response->header->statusCode);
        // exit;

        return $this->respondSuccess();
    }
}
