<?php

declare (strict_types = 1);

namespace App\Http\Api\Controllers;

use App\Http\Api\Core\Controllers\BaseController;
use App\Collections\User;
use App\Collections\ConfirmCode;

class ServicesController extends BaseController
{
    public function clearAction() : void
    {
        $users = User::find();
        foreach ($users as &$user) {
            $user->delete();
        }

        $codes = ConfirmCode::find();
        foreach ($codes as &$code) {
            $code->delete();
        }

        echo 'DONE!';
        exit;
    }

    public function testSigninAction() : void
    {

    }
}