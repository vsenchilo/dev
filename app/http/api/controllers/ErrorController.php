<?php

declare (strict_types = 1);

namespace App\Http\Api\Controllers;

use App\Http\Api\Core\Controllers\BaseController;
use Phalcon\Http\Response;
use Phalcon\Mvc\Dispatcher;

class ErrorController extends BaseController
{
    protected $errors = [];

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        parent::beforeExecuteRoute($dispatcher);

        $errors = $dispatcher->getParams('errors');

        foreach ($errors['errors'] as $field => $list) {
            $this->errors[$field] = $list;
        }
        unset($error, $errors);
    }

    public function error400Action() : Response
    {
        return $this->respondError(['errors' => $this->errors], 400);
    }

    public function error401Action() : Response
    {
        return $this->respondError(['message' => 'Необходима авторизация'], 401);
    }

    public function error403Action() : Response
    {
        return $this->respondError(['message' => 'Доступ запрещен'], 403);
    }

    public function error404Action() : Response
    {
        return $this->respondError(['message' => 'Страница не найдена'], 404);
    }
}
