<?php

declare (strict_types = 1);

namespace App\Http\Api\Controllers;

use App\Http\Api\Core\Controllers\BaseController;

use App\Middleware\AuthMiddleware;
use Phalcon\Mvc\Dispatcher;
use App\Collections\Telegram;

class TestController extends BaseController
{
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->addMiddleware(
            new AuthMiddleware,
            ['test']
        );

        parent::beforeExecuteRoute($dispatcher);
    }

    public function testAction()
    {
        $telegram = Telegram::findFirst(['conditions' => ['owner_id' => $this->getUserSession()->getUserId()]]);
        print_r($telegram);
        exit;
        echo 'ads';exit;
    }
}