<?php

declare (strict_types = 1);

namespace App\Http\Api\Controllers;

use App\Http\Api\Core\Controllers\BaseController;
use Phalcon\Http \{
    Response,
    Request
};
use App\Collections \{
    User,
    Session
};
use Services\Auth\Auth;
use App\Collections\ConfirmCode;
use Services\Generators\String\Generator as StrGenerator;
use Services\Messangers\Mailer;

class AuthController extends BaseController
{
    public function signinAction() : Response
    {
        $storage = $this->dispatcher->getParam('storage');
        $storage = sprintf(
            'Services\\Auth\\Storages\\%s',
            ucfirst($storage)
        );
        $storage = new $storage;

        $auth = new Auth();
        $auth->setStorageProvider($storage);

        $auth->requestSignin(
            $this->request->getPost('email'),
            $this->request->getPost('password')
        );

        if (!($auth->getSession() instanceof Session)) {
            return $this->respondError([], 401);
        }

        return $this->respondSuccess([
            'hash' => $storage->getAuthString(),
            'storage_type' => $auth->getSession()->getStorageType(),
            'storage_name' => $storage->getStorageName()
        ]);
    }

    public function signupAction() : Response
    {
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');

        $user = new User();
        $user
            ->setEmail($email)
            ->setPassword($password);

        if (false === $user->save()) {
            return $this->respondError(['errors' => $user->getErrors()], 400);
        }

        $generator = new StrGenerator();
        $mailer = new Mailer\Signup();

        $code = new ConfirmCode();
        $code
            ->setUser($user)
            ->setCodeGenerator($generator)
            ->setMessanger($mailer)
            ->setAction(ConfirmCode::ACTION_SIGNUP);

        $code->save();

        return $this->respondSuccess();
    }

    // public function emailConfirmAction() : Response
    // {
    //     $code = ConfirmCode::findOne(null, ['code' => $this->request->getPost('code')]);

    //     if (null === $code) {
    //         return $this->respondError(['message' => 'Не верный код подтверждения'], 400);
    //     }

    //     $user = User::findOne($code->getUserId());
    //     try {
    //         User::confirmEmail($user, $code);
    //     } catch (ConfirmException $e) {
    //         return $this->respondError(['errors' => $e->getErrors(), 'message' => $e->getMessage()], 500);
    //     }

    //     $session = (new BaseAuth())
    //         ->signIn($user, $this->request);

    //     return $this->respondSuccess([
    //         'token' => $session->getToken(),
    //         'storage_type' => $session->getStorageType(),
    //         'storage_name' => $session->getStorageName()
    //     ]);
    // }

    // public function resetPasswordAction(?User $user) : Response
    // {
    //     if (null === $user) {
    //         return $this->respondError(['message' => 'Пользователь не найден'], 400);
    //     }

    //     return $this->respondSuccess();
    // }
}
