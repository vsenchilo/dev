<?php

namespace App\Collections\Validators;

use Phalcon\Validation\Validator\Uniqueness;
use App\Collections;

trait User
{
    private $_validator;

    public function beforeValidation()
    {
        $this->_validator = new \Phalcon\Validation();

        $this->_validator->add(
            'email',
            new Uniqueness([
                'model' => new Collections\User,
                'message' => 'должно быть уникальным',
            ])
        );

    }

    public function validation()
    {
        return $this->validate(
            $this->_validator
        );
    }
}