<?php

namespace App\Collections\Validators;

use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\PresenceOf;
use App\Collections;

trait Channel
{
    private $_validator;

    public function beforeValidation()
    {
        $this->_validator = new \Phalcon\Validation();

        $this->_validator->add(
            'alias',
            new PresenceOf([
                'message' => 'не может быть пустым',
            ])
        );

        $this->_validator->add(
            'alias',
            new Regex([
                'pattern' => '/^[0-9a-z-_]{2,32}$/',
                'message' => 'должно содержать от 2 до 8 символов и может содержать символы 0-9a-z-_'
            ])
        );

        $this->_validator->add(
            'alias',
            new Uniqueness([
                'model' => new Collections\Channel,
                'message' => 'уже занято',
            ])
        );

        $this->_validator->add(
            'title',
            new PresenceOf([
                'message' => 'не может быть пустым',
            ])
        );
    }

    public function validation()
    {
        return $this->validate(
            $this->_validator
        );
    }
}