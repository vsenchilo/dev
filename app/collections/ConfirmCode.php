<?php

declare (strict_types = 1);

namespace App\Collections;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

use Services\Generators\iGenerator;
use Services\Messangers\iMessanger;

class ConfirmCode extends Collection
{
    const CONFIRM_CODE_LENGTH = 6;

    const DELIVERY_METHOD_EMAIL = 1;
    const DELIVERY_METHOD_PHONE = 2;
    const DELIVERY_METHOD_TELEGRAM = 4;
    const DELIVERY_METHOD_VIBER = 8;


    const ACTION_PASSWORD_RESET = 1;
    const ACTION_SIGNIN = 2;
    const ACTION_SIGNUP = 4;

    protected $user_id;
    protected $code;
    protected $action;

    private $codeGenerator;
    private $messanger;
    private $user;

    public function getSource() : string
    {
        return 'confirm_codes';
    }

    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            ['code', 'user_id', 'action'],
            new PresenceOf([
                'message' => [
                    'code' => ':field не указано',
                    'user_id' => ':field не указано',
                    'action' => ':field не указано'
                ]
            ])
        );

        return $this->validate($validator);
    }

    public function beforeValidationOnCreate()
    {
        $interfaces = class_implements($this->codeGenerator);

        if (isset($interfaces[iGenerator::class])) {
            $this->code = $this->codeGenerator->generate();
        }

        if ($this->user instanceof User) {
            $this->user_id = $this->user->getId();
        }
    }

    public function afterCreate()
    {
        $interfaces = class_implements($this->messanger);

        if (isset($interfaces[iMessanger::class])) {
            $this->messanger->setParam('email', $this->user->getEmail());
            $this->messanger->setParam('code', $this->code);
            $this->messanger->send();
        }
    }

    public function setCodeGenerator(iGenerator $codeGenerator) : self
    {
        $this->codeGenerator = $codeGenerator;

        return $this;
    }

    public function setMessanger(iMessanger $messanger) : self
    {
        $this->messanger = $messanger;

        return $this;
    }

    public function setUser(User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    public function setAction(int $action)
    {
        $this->action = $action;
    }
}
