<?php

declare (strict_types = 1);

namespace App\Collections;

use Phalcon\Mvc\MongoCollection;
use MongoDB\BSON\UTCDateTime;

abstract class Collection extends MongoCollection
{
    protected $created_at;
    protected $updated_at;

    private $_errors = [];

    public function initialize() {}

    public function beforeSave() {}

    public function beforeCreate()
    {
        $this->updated_at = $this->created_at = new UTCDateTime();
    }

    public function beforeUpdate()
    {
        $this->updated_at = new UTCDateTime();
    }

    public function getCreatedAt() : ?UTCDateTime
    {
        return $this->created_at;
    }

    public function getUpdatedAt() : ?UTCDateTime
    {
        return $this->updated_at;
    }

    public function getErrors() : array
    {
        $messages = $this->getMessages();

        foreach ($messages as &$message) {
            $this->_errors[$message->getField()][] = $message->getMessage();
        }
        unset ($message, $messages);

        return $this->_errors;
    }
}
