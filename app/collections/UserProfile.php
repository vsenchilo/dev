<?php

declare(strict_types=1);

namespace App\Collections;

use Services\Mongo\Collection;
use App\Collections\Validators;

class UserProfile extends Collection
{
    use Validators\UserProfile;

    function collectionName() : string
    {
        return 'users_profile';
    }
}
