<?php

declare (strict_types = 1);

namespace App\Collections;

use App\Exceptions\ChannelException;
use Services\Messangers\iMessanger;
use App\Exceptions\UserException;

class Channel extends Collection
{
    use Validators\Channel;

    private $messanger;

    protected $owner_id;
    protected $subscribers = [];
    protected $messangersSettings;
    protected $title;
    protected $alias;
    protected $description;
    protected $metaDescription;
    protected $metaKeywords;

    public function beforeSave()
    {
        parent::beforeSave();

        if (is_array($this->subscribers)) {
            $this->subscribers = array_values($this->subscribers);
        }

        // if (is_array($this->messangersSettings)) {
        //     $this->messangersSettings = array_values($this->messangersSettings);
        // }
    }

    public function setOwner(User $user) : self
    {
        if (null === $user->getId()) {
            throw new ChannelException('Can\'t assing empty owner');
        }

        $this->owner_id = $user->getId();

        return $this;
    }

    public function getOwner()
    {
        return $this->owner_id;
    }

    public function setMessanger(iMessanger $messanger) : self
    {
        $this->messanger = $messanger;

        return $this;
    }

    public function subscribe(User $user) : self
    {
        if (null === $user->getId()) {
            throw new ChannelException('Can\'t subscribe empty user');
        }

        if (!is_array($this->subscribers)) {
            $this->subscribers = [];
        }

        if (!empty($this->subscribers)) {
            foreach ($this->subscribers as $v) {
                if ($v['user_id'] == $user->getId()) {
                    return $this;
                }
            }
        }

        $this->subscribers[] = [
            'user_id' => $user->getId()
        ];

        try {
            $user->subscribe($this);
            $user->save();
        } catch (\Throwable $e) {
            unset($this->subscribers[count($this->subscribers) - 1]);
            throw new ChannelException($e->getMessage());
        }

        return $this;
    }

    // public function unsubscribe() : self
    // {
    //     return $this;
    // }

    public function sendSubscribeMessage() : self
    {
        $interfaces = class_implements($this->messanger);
        if (isset($interfaces[iMessanger::class])) {
            $this->messanger->setParam('email', $this->user->getEmail());
            $this->messanger->setParam('channel_title', $this->getTitle());
            $this->messanger->send();
        }

        return $this;
    }

    // public function setUnsubscribeMessage() : self
    // {
    //     return $this;
    // }

    public function setTitle(string $title) : self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle() : string
    {
        return $this->title ?? '';
    }

    // public function setTelegramApi(string $token) : self
    // {
    //     if (null === $this->messangersSettings) {
    //         $this->messangersSettings = new \stdClass();
    //     }

    //     $this->messangersSettings->telegram = $token;

    //     return $this;
    // }

    public function setDescription(string $description) : self
    {
        $description = trim($description);
        $description = htmlspecialchars($description);

        $this->description = $description;

        return $this;
    }

    public function getDescription() : string
    {
        return $this->description ?? '';
    }

    public function setMetaDescription(string $description) : self
    {
        $description = trim($description);
        $description = htmlspecialchars($description);

        $this->metaDescription = $description;

        return $this;
    }

    public function getMetaDescription() : string
    {
        return $this->metaDescription ?? '';
    }

    public function setMetaKeywords(string $keywords) : self
    {
        $keywords = trim($keywords);
        $keywords = htmlspecialchars($keywords);

        $this->metaKeywords = $keywords;

        return $this;
    }

    public function getMetaKeywords() : string
    {
        return $this->metaKeywords ?? '';
    }

    public function setAlias(string $alias) : self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getAlias() : string
    {
        return $this->alias ?? '';
    }

    public function getSubscribers() : array
    {
        return $this->subscribers ?? [];
    }

    // public function getSubscribersCount() : int
    // {
    //     return isset($this->subscribers) ? count($this->subscribers) : 0;
    // }
}
