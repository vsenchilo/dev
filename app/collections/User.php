<?php

declare(strict_types=1);

namespace App\Collections;

use App\Exceptions\ChannelException;
use App\Exceptions\UserException;

class User extends Collection
{
    use Validators\User;

    const PASSWORD_MIN_LENGTH = 8;

    protected $status;
    protected $verified;
    protected $email;
    protected $password;
    protected $channels;
    protected $subscriptions;

    public function getSource() : string
    {
        return 'users';
    }

    public function beforeSave()
    {
        parent::beforeSave();

        if (is_array($this->channels)) {
            $this->channels = array_values($this->channels);
        }

        if (is_array($this->subscriptions)) {
            $this->subscriptions = array_values($this->subscriptions);
        }
    }

    public function subscribe(Channel $channel) : self
    {
        if (!$this->getId()) {
            throw new UserException('Subscribers not defined');
        }

        if (!$channel->getId()) {
            throw new ChannelException('Channel not defined');
        }

        if (!is_array($this->subscriptions)) {
            $this->subscriptions = [];
        }

        if (!in_array($channel->getId(), $this->subscriptions)) {
            $this->subscriptions[] = $channel->getId();
        }

        return $this;
    }

    public function getSubscriptions() : array
    {
        $subscriptions = [];
        if (!empty($this->subscriptions)) {
            $subscriptions = Channel::find(['conditions' => ['_id' => ['$in' => $this->subscriptions]]]);
        }
        return $subscriptions;
    }

    public static function getRegistred(string $email, string $password) : ?self
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $user = self::findFirst(['conditions' => ['email' => $email]]);

            if (!$user || !self::validatePassword($user, $password)) {
                $user = null;
            }
        }

        return $user;
    }

    public function getChannels() : array
    {
        $channels = Channel::find(
            [
                'conditions' => [
                    'owner_id' => $this->getId(),
                ],
                'sort' => [
                    '_id' => -1
                ]
            ]
        );

        return $channels;
    }

    public function addChannel(Channel $channel) : self
    {
        if (null === $this->getId()) {
            throw new UserException('Cant assign channel for empty user');
        }

        if (null === $channel->getId()) {
            if (!$channel->save()) {
                throw new ChannelException('Can\'t save channel');
            }
        }

        if (null === $this->channels) {
            $this->channels = [];
        }

        if (!in_array($channel->getId(), $this->channels)) {
            $this->channels[] = $channel->getId();
        }

        return $this;
    }

    public function removeChannel() : self
    {}

    public function setEmail(string $email) : self
    {
        $this->email = $email;

        return $this;
    }

    public function setPassword(string $password) : self
    {
        $this->password = password_hash($password, PASSWORD_BCRYPT);

        return $this;
    }

    public function getPassword() : string
    {
        return $this->password ?? '';
    }

    public function getEmail() : string
    {
        return $this->email ?? '';
    }

    public static function validatePassword(self $user, string $password) : bool
    {
        return password_verify($password, $user->getPassword());
    }

    // public function isConfirmed() : bool
    // {
    //     return isset($this->verified) && isset($this->verified->email) && $this->verified->email === true;
    // }
}
