<?php

declare (strict_types = 1);

namespace App\Collections;

class Session extends Collection
{
    private $user;

    protected $user_id;
    protected $token;
    protected $auth_hash;
    protected $session_ttl;
    protected $storage_type;

    public $user_ip;
    public $user_client;

    public function getSource()
    {
        return 'sessions';
    }

    public function beforeSave()
    {
        if ($this->getUser() instanceof User) {
            $this->user_id = $this->getUser()->getid();
        }
    }

    public function setUser(User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    public function setToken(string $token) : self
    {
        $this->token = $token;

        return $this;
    }

    public function setAuthHash(string $hash) : self
    {
        $this->auth_hash = $hash;

        return $this;
    }

    public function setStorageType(string $storageType) : self
    {
        $this->storage_type = $storageType;

        return $this;
    }

    public function setSessionTTL(int $ttl) : self
    {
        $this->session_ttl = $ttl;

        return $this;
    }

    public function getStorageType() : string
    {
        return $this->storage_type;
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getUser() : ?User
    {
        return $this->user;
    }

    public function getToken() : ?string
    {
        return $this->token;
    }

    public function getAuthHash() : ?string
    {
        return $this->auth_hash;
    }
}
