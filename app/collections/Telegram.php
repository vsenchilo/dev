<?php

declare (strict_types = 1);

namespace App\Collections;

use Exceptions\API\TelegramException;


class Telegram extends Collection
{
    protected $tokens = [];
    protected $channel_id;
    protected $owner_id;
    protected $subscribers = [];

    public function getSource() : string
    {
        return 'telegram';
    }

    public function setChannel(Channel $channel) : self
    {
        if (null != $this->channel_id && (string)$this->channel_id !== (string)$channel->getId()) {
            throw new TelegramException('Бот уже закреплен за другим каналом');
        }

        $this->channel_id = $channel->getId();

        return $this;
    }

    public function unsetChanne() : self
    {
        $this->channel_id = null;

        return $this;
    }

    public function setToken(string $token) : self
    {
        if (!is_array($this->tokens)) {
            $this->tokens = [];
        }

        if (!in_array($token, $this->tokens)) {
            $this->tokens[] = $token;
        }

        return $this;
    }

    public function getTokens() : array
    {
        return $this->tokens ?? [];
    }

    public function setOwner(User $user) : self
    {
        $this->owner_id = $user->getId();

        return $this;
    }

    public function subscribe(User $subscriber, string $token, string $chatId)
    {
        $subscriberId = (string)$subscriber->getId();

        if (!is_array($this->subscribers)) {
            $this->subscribers = [];
        }

        if (!isset($this->subscribers[$token])) {
            $this->subscribers[$token] = [];
        }

        if (!isset($this->subscribers[$token][$subscriberId])) {
            $this->subscribers[$token][$subscriberId] = $chatId;
        }

        return $this;
    }

    public function getSubscribers() : array
    {
        return $this->subscribers ?? [];
    }
}
