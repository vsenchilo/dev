<?php

declare (strict_types = 1);

namespace App\Collections;

class TelegramUsers extends Collection
{
    protected $telegram_user_id;
    protected $telegram_token;
    protected $user_id;
    protected $email;
    protected $confirm_code;
    protected $confirmed;
    protected $messages;

    public function getSource() : string
    {
        return 'telegram_users';
    }

    public function setUserId(string $id) {}
    public function setTelegramUserId(string $id) {}
    public function setEmail (string $email) {}
}