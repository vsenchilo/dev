<?php

declare(strict_types=1);

namespace Core\Traits\Http;

trait View
{
    function setVar(string $var, $val) : void
    {
        $this->view->{$var} = $val;
    }

    function setVars(array $vars) : void
    {
        foreach($vars as $k => &$v) {
            $this->view->{$k} = $v;
        }
    }
}