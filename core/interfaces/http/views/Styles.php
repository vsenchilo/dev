<?php

declare(strict_types=1);

namespace Core\Interfaces\Http\Views;

interface Styles
{
    function setStyle(string $style) : void;
    function setStyles(array $styles) : void;
    function getScripts() : array;
    function setVars(array $vars) : void;
}