<?php

declare(strict_types=1);

namespace Core\Interfaces\Http\Views;

interface Scripts
{
    function setScript(string $script) : void;
    function setScripts(array $scripts) : void;
    function getScripts() : array;
    function setVars(array $vars) : void;
}