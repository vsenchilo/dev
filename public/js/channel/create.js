// Create channel

window.channel = window.channel || {};
window.channel.fn = window.channel.fn || {};

$.extend(window.channel.fn, {
    // onStepChanging(event, currentIndex, newIndex) {
    //     let status = true;
    //     switch (currentIndex) {
    //         case 0:
    //             let title = $('#channel-title');
    //             let alias = $('#channel-alias');

    //             if (!title.val().length) {
    //                 status = false;
    //                 title.addClass('error');
    //                 title.parent().append('<label for="' + title.attr('id') + '" class="error-input-msg">Укажите название канала</span>');
    //             }

    //             if (!alias.val().length) {
    //                 status = false;
    //                 alias.addClass('error');
    //                 alias.parent().append('<label for="' + alias.attr('id') + '" class="error-input-msg">Укажите alias канала</span>');
    //             } else if (!(/^[a-zA-Z-_]+$/.test(alias.val()))) {
    //                 status = false;
    //                 alias.addClass('error');
    //                 alias.parent().append('<label for="' + alias.attr('id') + '" class="error-input-msg">Может содержать только символы латинского алфавита и знаки "-" и "_"</span>');
    //             }

    //             break;
    //     }

    //     // window.channel.Create.wysiwygDescription
    //     return status;
    // },
    create: function ($, fields, options) {
        $.ajax({
            url: options.url,
            type: options.method,
            dataType: options.dataType,
            data: {
                'title': fields.title.val(),
                'alias': fields.alias.val(),
                'description': fields.description.val(),
                'metaKeywords': fields.metaKeywords.val(),
                'metaDescription': fields.metaDescription.val(),
            },
            beforeSend: function (xhr) {
                for (let field in fields) {
                    fields[field].prop('disabled', true);
                    fields[field].removeClass('error');
                    fields[field].parent().find('label.error-input-msg').remove();
                }
            },
            statusCode: {
                200: function () {
                },
                400: function (xhr) {
                    let response = $.parseJSON(xhr.responseText);
                    for (field in response.errors) {
                        fields[field].addClass('error');

                        fields[field].parent().append('<label for="' + fields[field].attr('id') + '" class="error-input-msg">' + response.errors[field][0] + '</span>');
                    }
                }
            }
        }).fail(function () {
        }).done(function () {
        }).always(function () {
            for (let field in fields) {
                fields[field].prop('disabled', false);
            }
        });
    }
});

(function (channel, $) {

    channel = channel || {};

    let initialized = false;

    $.extend(channel, {
        Create: {
            wysiwygDescription: null,
            options: {
                url: '/api/v1/channel/create/a/cookies',
                method: 'POST',
                dataType: 'json',
            },
            fields: {
                'title': $('#channel-title'),
                'alias': $('#channel-alias'),
                'description': $('#channel-description'),
                'metaKeywords': $('#channel-meta-keywords'),
                'metaDescription': $('#channel-meta-description'),
                'submit': $('#create-channel'),
            },
            initialize: function () {
                if (initialized) {
                    return this;
                }
                let self = this;

                initialized = true;

                // $('#create-channel-form-wizzar').steps({
                //     cssClass: "form-wizard channel-wizard",
                //     headerTag: "h3",
                //     bodyTag: "section",
                //     transitionEffect: $.fn.steps.transitionEffect.fade,
                //     autoFocus: true,
                //     onStepChanging: channel.fn.onStepChanging,
                //     transitionEffectSpeed: 100,
                //     actionContainerTag: 'footer',
                //     startIndex: 0,
                //     labels: {
                //         current: "",
                //         finish: "Сохранить",
                //         next: "Далее",
                //         previous: "Назад",
                //     }
                // });

                // this.wysiwygDescription = new Quill('#channel-description', {
                //     theme: 'snow',
                //     placeholder: 'Описание канала',
                // });

                this.events();

                return this;
            },
            events: function () {
                let self = this;

                // for (let field in this.fields) {
                $(document.body).on('focus', 'input.form-control', function () {
                    $(this).removeClass('error');
                    $(this).parent().find('label.error-input-msg').remove();
                });

                self.fields.submit.on('click', function (e) {
                    channel.fn.create($, self.fields, self.options);
                });
            }
        }
    });
}).apply(this, [window.channel, jQuery]);