$('#send-message').on('click', function () {
    $.ajax({
        url: '/api/v1/channel/' + $('#channel-id').val() + '/message/send/a/cookies',
        method: 'POST',
        dataType: 'json',
        data: {
            message: $('#channel-message').val()
        }
    })
});
