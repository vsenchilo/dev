// Create channel

window.telegram = {};

window.telegram.fn = {
    connect: function ($, fields, options) {
        $.ajax({
            url: options.url,
            type: options.method,
            dataType: options.dataType,
            data: {
                'token': fields.token.val(),
                'channel': fields.channelId.val(),
            },
        })
    }
};

(function (telegram, $) {

    telegram = telegram || {};

    let initialized = false;

    $.extend(telegram, {
        Connect: {
            options: {
                url: '/api/v1/channel/telegram/connect/a/cookies',
                method: 'POST',
                dataType: 'json',
            },
            fields: {
                'token': $('#telegram-token'),
                'channelId': $('#channel-id'),
                'submit': $('#connect-telegram'),
            },
            initialize: function () {
                if (initialized) {
                    return this;
                }

                initialized = true;

                this.events();

                return this;
            },
            events: function () {
                let self = this;

                for (let field in this.fields) {
                    self.fields[field].on('focus', function () {
                        self.fields[field].removeClass('error');
                        self.fields[field].parent().find('label.error-input-msg').remove();
                    });
                }

                self.fields.submit.on('click', function (e) {
                    telegram.fn.connect($, self.fields, self.options);
                });
            }
        }
    });
}).apply(this, [window.telegram, jQuery]);