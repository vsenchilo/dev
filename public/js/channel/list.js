(function($){
    let channelContainerPrototype = $('#channel-prototype');
    let channelsContainer = $('#channels-list');

    function loadChannel() {
        channelsContainer.html('');

        $.ajax('/api/v1/channel/list/a/cookies', {
            dataType: 'json',
            success: function (response) {
                for (channel in response.channels) {
                    let container = channelContainerPrototype.clone();
                    container.removeAttr('id');
                    container.find('.channel-name').html(response.channels[channel]['title']);
                    container.find('.telegram-settings').attr('data-id', response.channels[channel]['id'])
                    container.find('.telegram-settings').attr('data-title', response.channels[channel]['title'])
                    channelsContainer.append(container);
                }

                $('.telegram-settings').animatedModal({
                    color: '#FFF',
                    animatedIn: 'slideInDown',
                    animatedOut: 'slideInDown',
                    animationDuration: '.4s',
                    beforeOpen: function (e) {
                        $(this).find('h3.channel-title').html($(e).data('title'));
                    },
                    afterOpen: function (e) {
                    },
                    beforeClose: function () {
                    },
                    afterClose: function () {
                    }
                });
            }
        });
    }

    $('#create-channel').animatedModal({
        color: '#FFF',
        animatedIn: 'slideInDown',
        animatedOut: 'slideInDown',
        animationDuration: '.4s',
        beforeOpen: function () {
        },
        afterOpen: function () {
        },
        beforeClose: function () {
        },
        afterClose: function () {
        }
    });

    $('#create-channel-form-wizzar').steps({
        cssClass: "form-wizard channel-wizard",
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: $.fn.steps.transitionEffect.fade,
        autoFocus: true,
        onStepChanging: function (event, currentIndex, newIndex) {
            let status = true;
            switch (currentIndex) {
                case 0:
                    let title = $('#channel-title');
                    let alias = $('#channel-alias');

                    if (!title.val().length) {
                        status = false;
                        title.addClass('error');
                        title.parent().append('<label for="' + title.attr('id') + '" class="error-input-msg">Укажите название канала</span>');
                    }

                    if (!alias.val().length) {
                        status = false;
                        alias.addClass('error');
                        alias.parent().append('<label for="' + alias.attr('id') + '" class="error-input-msg">Укажите alias канала</span>');
                    } else if (!(/^[a-zA-Z-_]+$/.test(alias.val()))) {
                        status = false;
                        alias.addClass('error');
                        alias.parent().append('<label for="' + alias.attr('id') + '" class="error-input-msg">Может содержать только символы латинского алфавита и знаки "-" и "_"</span>');
                    }
                    break;
            }
            // window.channel.Create.wysiwygDescription
            return status;
        },
        onFinishing: function (event, currentIndex) {
            $.ajax('/api/v1/channel/create/a/cookies', {
                method: 'POST',
                dataType: 'json',
                data: {
                    'title': $('#channel-title').val(),
                    'alias': $('#channel-alias').val(),
                    'description': $('#channel-description').val(),
                    'metaKeywords': $('#channel-meta-keywords').val(),
                    'metaDescription': $('#channel-meta-description').val()
                }
            });
        },
        transitionEffectSpeed: 100,
        actionContainerTag: 'footer',
        startIndex: 0,
        labels: {
            current: "",
            finish: "Сохранить",
            next: "Далее",
            previous: "Назад",
        }
    });

    $(document.body).on('focus', 'input.form-control', function () {
        $(this).removeClass('error');
        $(this).parent().find('label.error-input-msg').remove();
    });

    $(document.body).on('click', '.tellegram-settings', function() {

    });

    setTimeout(function() {
        loadChannel();
        $('#channel-settings-container').css('display', 'block');
        $('#telegram-settings-container').css('display', 'block');

    }, 200);
})(jQuery);