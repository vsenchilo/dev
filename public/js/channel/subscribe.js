// Create channel

window.channel = window.channel || {};
window.channel.fn = window.channel.fn || {};

$.extend(window.channel.fn, {
    subscribe: function ($, channel, options) {
        $.ajax({
            url: options.url,
            type: options.method,
            dataType: options.dataType,
            data: {
                channel: channel
            },
        });
    }
});

(function (channel, $) {

    channel = channel || {};

    let initialized = false;

    $.extend(channel, {
        Subscribe: {
            options: {
                url: '/api/v1/channel/subscribe/a/cookies',
                method: 'POST',
                dataType: 'json',
            },
            initialize: function () {
                if (initialized) {
                    return this;
                }

                initialized = true;

                this.events();

                return this;
            },
            events: function () {
                let self = this;

                $(document.body).on('click', '.channel-subscribe', function () {
                    channel.fn.subscribe($, $(this).data('channel'), self.options);
                });
            }
        }
    });
}).apply(this, [window.channel, jQuery]);