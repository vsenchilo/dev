// Auth
window.auth = {};

window.auth.fn = {
    passwordMeter: function (password) {
        let strongRegex = new RegExp("^(?=.{8,})(?=.*[A-ZА-Я])(?=.*[a-zа-я])(?=.*[0-9])(?=.*\\W).*$", "g");
        let mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-ZА-Я])(?=.*[a-zа-я]))|((?=.*[A-ZА-Я])(?=.*[0-9]))|((?=.*[a-zа-я])(?=.*[0-9]))).*$", "g");
        let enoughRegex = new RegExp("(?=.{8,}).*", "g");

        if (password.length == 0) {
            return 0;
        } else if (!enoughRegex.test(password)) {
            return 1;
        } else if(strongRegex.test(password)) {
            return 4;
        } else if (mediumRegex.test(password)) {
            return 3
        } else {
            return 2;
        }
    },
    comparePassword: function (password, cpassword) {
        return password === cpassword;
    },
    validatePassword: function (field, text) {
        let strength = {
            0: 'Недопустимы',
            1: 'Короткий',
            2: 'Слабый',
            3: 'Средний',
            4: 'Надежный'
        }
        let result = this.passwordMeter(field.val());

        switch (result) {
            case 0:
            case 1:
                text
                    .removeClass('alert-tertiary alert-warning alert-info alert-success')
                    .addClass('alert-danger')
                    .html(strength[result])
                break;
            case 2:
                text
                    .removeClass('alert-tertiary alert-danger alert-info alert-success')
                    .addClass('alert-warning')
                    .html(strength[result])
                break;
            case 3:
                text
                    .removeClass('alert-tertiary alert-danger alert-warning alert-success')
                    .addClass('alert-info')
                    .html(strength[result])
                break;
            case 4:
                text
                    .removeClass('alert-tertiary alert-info alert-warning alert-danger')
                    .addClass('alert-success')
                    .html(strength[result])
                break;
        }
    },
    validateEmail: function (email) {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    },
    signout: function () {

    },
    signup: function ($, fields, options, validators) {
        let self = this;

        $.ajax({
            url: options.url,
            type: options.method,
            dataType: options.dataType,
            data: {
                'email': fields.email.val(),
                'password': fields.password.val(),
                'cpassword': fields.cpassword.val(),
            },
            beforeSend: function (xhr) {
                let valid = true;
                let html = '';

                options.errorsWrap
                    .html('')
                    .slideUp({
                        duration: 90
                    });

                let emailTest = validators.email(fields.email.val());
                let passwordTest = validators.password(fields.password.val());

                if (!emailTest) {
                    html += '<li>Email не валидный</li>';
                    fields.email.addClass('error');
                    valid = false;
                }

                if (passwordTest === 0 || passwordTest === 1) {
                    html += '<li>Недопустимый пароль</li>';
                    fields.password.addClass('error');
                    valid = false;
                }

                if (!valid) {
                    xhr.abort();
                    options.errorsWrap
                        .html(html)
                        .slideDown({
                            duration: 90
                        });
                } else {
                    for (let field in fields) {
                        fields[field].prop('disabled', true);
                    }
                }
            },
            statusCode: {
                200: function () {
                },
                400: function (xhr) {
                    let response = $.parseJSON(xhr.responseText);
                    let html = '';

                    for (let errors in response.errors) {
                        fields[errors].addClass('error');

                        for (let error in response.errors[errors]) {
                            html += '<li>' + response.errors[errors][error] + '</li>';
                        }
                    }
                    options.errorsWrap.html(html).slideDown();

                    setTimeout(function () {
                        for (let field in fields) {
                            fields[field].prop('disabled', false);
                        }
                    }, 500);
                }
            }
        });
    },

    signin: function ($, fields, options, validators) {
        $.ajax({
            url: options.url,
            type: options.method,
            dataType: options.dataType,
            data: {
                'email': fields.email.val(),
                'password': fields.password.val()
            },
            beforeSend: function (xhr) {
                let valid = true;
                let html = '';

                options.errorsWrap
                    .html('')
                    .slideUp({
                        duration: 90
                    });

                let emailTest = validators.email(fields.email.val());

                if (!emailTest) {
                    html += '<li>Email не валидный</li>';
                    fields.email.addClass('error');
                    valid = false;
                }

                if (!valid) {
                    xhr.abort();
                    options.errorsWrap
                        .html(html)
                        .slideDown({
                            duration: 90
                        });
                } else {
                    for (let field in fields) {
                        fields[field].prop('disabled', true);
                    }
                }
            },
            statusCode: {
                200: function () {
                    window.location.href = '/';
                },
                401: function (xhr) {
                    let response = $.parseJSON(xhr.responseText);
                    let html = '';

                    for (let errors in response.errors) {
                        fields[errors].addClass('error');

                        for (let error in response.errors[errors]) {
                            html += '<li>' + response.errors[errors][error] + '</li>';
                        }
                    }
                    options.errorsWrap
                        .html(html).
                        slideDown();

                    setTimeout(function () {
                        for (let field in fields) {
                            fields[field].prop('disabled', false);
                        }
                    }, 500);
                }
            }
        });
    }
};

(function (auth, $) {

    auth = auth || {};

    let initialized = false;

    $.extend(auth, {
        Signup: {
            options: {
                url: '/api/v1/signup/a/cookies',
                method: 'POST',
                dataType: 'json',
                errorsWrap: $('#header-signup-error'),
                successWrap: $('#header-signup-success')
            },
            fields: {
                password: $('#signup-password'),
                cpassword: $('#signup-cpassword'),
                email: $('#signup-email'),
                submit: $('#sign-up')
            },
            validators: {
                'email': auth.fn.validateEmail,
                'password': auth.fn.passwordMeter
            },
            defaults: {
                'passwordStrengthText': $('#header-password-strength-text')
            },
            initialize: function () {
                if (initialized) {
                    return this;
                }

                initialized = true;

                this.events();

                return this;
            },
            events: function () {
                let self = this;

                 this.fields.password.on('input', function () {
                     auth.fn.validatePassword(
                         self.fields.password,
                         self.defaults.passwordStrengthText
                     );
                 });

                 for (let field in this.fields) {
                     self.fields[field].on('focus', function () {
                         self.fields[field].removeClass('error');
                     });
                 }

                self.fields.submit.on('click', function (e) {
                    auth.fn.signup($, self.fields, self.options, self.validators);
                });
            }
        }
    });
}).apply(this, [window.auth, jQuery]);

(function (auth, $) {

    auth = auth || {};

    let initialized = false;

    $.extend(auth, {
        Signin: {
            options: {
                url: '/api/v1/signin/a/cookies',
                method: 'POST',
                dataType: 'json',
                errorsWrap: $('#header-signin-error'),
                successWrap: $('#header-signin-success')
            },
            fields: {
                email: $('#signin-email'),
                password: $('#signin-password'),
                submit: $('#sign-in')
            },
            validators: {
                'email': auth.fn.validateEmail,
            },
            initialize: function () {
                if (initialized) {
                    return this;
                }

                initialized = true;

                this.events();

                return this;
            },
            events: function () {
                let self = this;

                self.fields.submit.on('click', function (e) {
                    auth.fn.signin($, self.fields, self.options, self.validators);
                });
            }
        }
    });
}).apply(this, [window.auth, jQuery]);
