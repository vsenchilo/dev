{% extends 'main.volt' %}

{% block content %}
<div class="row justify-content-around">
    <div class="col-md-10 col-lg-8 ml-auto">
        {% for channel in channels %}
        <article class="custom-post-style-2 mb-5">
            <h3 class="text-color-dark font-weight-bold text-uppercase mb-2">{{ channel.getTitle() }}</h3>
            <p class="custom-text-color-1">{{ channel.getDescription() }}</p>
            <div class="post-links">
                {% if user %}
                    {% if user.getId() != channel.getOwner() %}
                        <a style="cursor: pointer;" class="text-decoration-none text-uppercase text-color-dark font-weight-semibold custom-primary-hover float-left channel-subscribe" data-channel="{{ channel.getId() }}">Подписаться<i class="fa fa-long-arrow-right text-color-primary ml-2"></i></a>
                    {% else %}
                        <a href="{{ url('/channels/' ~ channel.getId()) }}" class="text-uppercase text-color-dark font-weight-semibold custom-primary-hover float-left">Редактировать</a>
                    {% endif %}
                {% else %}
                    Что бы подписаться на канал, необходимо авторизоваться
                {% endif %}
            </div>
        </article>
        {% endfor %}
    </div>
    <div class="col-lg-3">--</div>
</div>
{% endblock %}

{% block scripts %}
    {{ super() }}
    <script type="text/javascript" src="{{ url('js/channel/subscribe.js') }}"></script>
{% endblock %}