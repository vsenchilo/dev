<div class="header-column justify-content-end">
    <div class="header-row">
        <div class="header-nav">
            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 custom-header-nav-main-dark-style">
                <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                        <li class="dropdown dropdown-mega dropdown-mega-signin signin" id="headerAccount">
                            <a class="dropdown-item dropdown-toggle" href="#">
                                Вход / Регистрация
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="dropdown-mega-content">
                                        <div class="row">
                                            <div class="col">
                                                <div class="signin-form">
                                                    <span class="dropdown-mega-sub-title">Авторизация</span>

                                                    <form action="/" id="frmSignIn" method="post">
                                                        <div class="form-row">
                                                            <div class="form-group col">
                                                                <label>E-mail</label>
                                                                <input type="email" value="" id="signin-email" class="form-control form-control-lg" tabindex="1">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col">
                                                                <a class="float-right mt-0 p-0" id="headerRecover" href="#">Забыли пароль?</a>
                                                                <label>Пароль</label>
                                                                <input type="password" autocomplete="disable" id="signin-password" value="" class="form-control form-control-lg" tabindex="2">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-lg-6 pl-0">
                                                            </div>
                                                            <div class="form-group col-lg-6 pr-0">
                                                                <input type="button" value="Вход" id="sign-in" class="btn btn-primary float-right mb-3">
                                                            </div>
                                                        </div>
                                                    </form>

                                                    <p class="sign-up-info">Еще не зарегестрированы?
                                                        <a href="#" id="headerSignUp" class="p-0 m-0 ml-1">Регистрация</a>
                                                    </p>

                                                    <ul id="header-signin-error" class="alert alert-danger" style="display: none"></ul>
                                                </div>

                                                <div class="signup-form" id="signup-form">
                                                    <span class="dropdown-mega-sub-title">Регистрация</span>

                                                    <form action="/" id="frmSignUp" method="post">
                                                        <div class="form-row">
                                                            <div class="form-group col">
                                                                <label>E-mail</label>
                                                                <input type="email" autocomplete="disable" id="signup-email" value="" class="form-control form-control-lg">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-lg-6">
                                                                <label>Пароль</label>
                                                                <input type="password" autocomplete="disable" id="signup-password" value="" title="Пароль может содержать любые символы и буквы русского и английского алфавита"
                                                                    class="form-control form-control-lg">
                                                            </div>
                                                            <div class="form-group col-lg-6">
                                                                <label>Повторите пароль</label>
                                                                <input type="password" autocomplete="disable" id="signup-cpassword" value="" class="form-control form-control-lg">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="col-lg-6 col-sm-12">
                                                                <p id="header-password-strength-text" class="alert alert-tertiary text-center" style="padding: 5px 0">Надежность
                                                                    пароля</p>
                                                            </div>
                                                            <div class="col-lg-6 col-sm-12">
                                                                <input type="button" value="Регистрация" id="sign-up" class="btn btn-primary float-right mb-3">
                                                            </div>
                                                        </div>
                                                    </form>

                                                    <p class="log-in-info">Уже зарегестрированы?
                                                        <a href="#" id="headerSignIn" class="p-0 m-0 ml-1">Вход</a>
                                                    </p>

                                                    <ul id="header-signup-error" class="alert alert-danger" style="display: none"></ul>
                                                    <div id="header-signup-success" class="alert alert-success" style="display: none"></div>
                                                </div>

                                                <div class="recover-form">
                                                    <span class="dropdown-mega-sub-title">Восстановить пароль</span>
                                                    <p>Укажите Ваш E-mail и вам будут высланы инструкции для восстановления пароля.</p>

                                                    <form action="/" id="frmResetPassword" method="post">
                                                        <div class="form-row">
                                                            <div class="form-group col">
                                                                <label>E-mail Address</label>
                                                                <input type="text" value="" class="form-control form-control-lg">
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col">
                                                                <input type="submit" value="Submit" class="btn btn-primary float-right mb-3" data-loading-text="Loading...">
                                                            </div>
                                                        </div>
                                                    </form>

                                                    <p class="log-in-info">Уже зарегестрированы?
                                                        <a href="#" id="headerRecoverCancel" class="p-0 m-0 ml-1">Вход</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <a class="text-color-dark text-decoration-none d-none d-sm-block" target="_blank">
                <span class="custom-toll-free float-right font-weight-bold">
                    <i class="fa fa-mobile"></i>
                    <span class="toll-free-title text-uppercase font-weight-normal"></span>
                    <span class="toll-free-number font-weight-bold">---</span>
                </span>
            </a>
            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                <i class="fa fa-bars"></i>
            </button>
        </div>
    </div>
</div>