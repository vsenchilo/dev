<div class="header-column justify-content-end">
    <div class="header-row">
        <div class="header-nav">
            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 custom-header-nav-main-dark-style">
                <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                        <li>
                            <a class="nav-link" href="/profile/channels">Мои каналы</a>
                        </li>
                        <li>
                            <a class="nav-link" href="/profile/channels/subscriptions">Мои подписки</a>
                        </li>
                        <li class="dropdown dropdown-mega">
                            <a class="dropdown-item dropdown-toggle" href="#">
                                Профиль
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <div class="dropdown-mega-content">
                                        <div class="row">
                                            <div class="col">
                                                йцуйцу
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <a class="text-color-dark text-decoration-none d-none d-sm-block" target="_blank">
                <span class="custom-toll-free float-right font-weight-bold">
                    <i class="fa fa-mobile"></i>
                    <span class="toll-free-title text-uppercase font-weight-normal"></span>
                    <span class="toll-free-number font-weight-bold">---</span>
                </span>
            </a>
            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                <i class="fa fa-bars"></i>
            </button>
        </div>
    </div>
</div>