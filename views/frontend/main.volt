<!DOCTYPE html>
<html lang="{% if documentLang is defined %}{{ documentLang }}{% else %}ru{% endif %}">
<head>
    <title>in4line :: доставка информации</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Быстрая доставка информации">
    <meta name="keywords" content="торговые сигналы, форекс сигналы, сигналы, торговые сигналы форекс, доставка информации, доставка данных, in4line, инфолайн, быстрая доставка информации, быстрая доставка данных">
    {% block styles %}
    <link rel="stylesheet" href="{{ url('vendor/bootstrap/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ url('vendor/font-awesome/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ url('vendor/animate/animate.min.css') }}" />
    <link rel="stylesheet" href="{{ url('vendor/simple-line-icons/css/simple-line-icons.min.css') }}" />
    <link rel="stylesheet" href="{{ url('css/theme.css') }}" />
    <link rel="stylesheet" href="{{ url('css/theme-elements.css') }}" />
    <link rel="stylesheet" href="{{ url('css/demos/demo-finance.css') }}" />
    <link rel="stylesheet" href="{{ url('css/skins/skin-finance.css') }}" />
    <link rel="stylesheet" href="{{ url('css/custom.css') }}" />
    {% endblock %}
    <script type="text/javascript" src="{{ url('vendor/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('vendor/jquery.easing/jquery.easing.min.js') }}"></script>
</head>
<body class="page-{{ router.getControllerName() }} page-{{ router.getControllerName() }}-{{ router.getActionName() }} body">
<div class="body">
    <header id="header">
        <div class="header-body" style="top: 0px;">
            <div class="header-container container">
                <div class="header-row">
                    <div class="header-column">
                        <div class="header-row">
                            <div class="header-logo">
                            </div>
                        </div>
                    </div>
                    {% if user is defined and user %}
                        {% include 'partials/header/profile.block.volt' %}
                    {% else %}
                        {% include 'partials/header/auth.block.volt' %}
                    {% endif %}
                </div>
            </div>
        </div>
    </header>
    <div class="main" role="main">
        <section class="section section-no-border background-color-light m-0">
            <div class="container">
                {% block content %}{% endblock %}
            </div>
        </div>
    </div>
</div>

{% block scripts %}
<script type="text/javascript" src="{{ url('vendor/popper/umd/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/common/common.min.js') }}"></script>
<script type="text/javascript" src="{{ url('vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/theme.js') }}"></script>
<script type="text/javascript" src="{{ url('js/auth.js') }}"></script>
{% endblock %}

<script type="text/javascript" src="{{ url('js/theme.init.js') }}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131732691-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-131732691-1');
</script>
</body>
</html>