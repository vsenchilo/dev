{% extends 'main.volt' %}

{% block content %}
<div class="row">
    <div class="col">
        <section class="card card-admin">
            <header class="card-header">
                <h2>Создать уведомление для подписчиков</h2>
            </header>
            <div class="card-body">
                <form class="form-horizontal form-bordered">
                    <div class="form-group row">
                        <div class="col-lg-12 position-relative">
                            <input type="hidden" value="{{ channe_id }}" id="channel-id" />
                            <textarea type="text" class="form-control" id="channel-message"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <footer class="card-footer">
                <button type="button" class="btn btn-primary float-right" id="send-message">Отправить</button>
            </footer>
        </section>
    </div>
</div>
{% endblock %}

{% block scripts %}
    {{ super() }}
    <script type="text/javascript" src="{{ url('js/channel/sendMessage.js') }}"></script>
{% endblock %}