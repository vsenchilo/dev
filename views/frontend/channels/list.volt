{% extends 'main.volt' %}

{% block content %}
<div class="row justify-content-around mb-5">
    <div class="col-md-12 ml-auto">
        <a href="{{ url('profile/channels/create') }}" class="btn btn-info btn-block">Создать новый канал</a>
    </div>
</div>
<div class="row justify-content-around">
    <div class="col-md-12 ml-auto">
        {% for channel in channels %}
        <article class="custom-post-style-2 mb-5">
            <h3 class="text-color-dark font-weight-bold text-uppercase mb-2 pl-0">{{ channel.getTitle() }}</h3>
            <p class="custom-text-color-1">{{ channel.getDescription() }}</p>
            <div class="post-links">
                <a href="{{ url('/profile/channels/' ~ channel.getId()) }}" class="text-decoration-none text-uppercase text-color-dark font-weight-semibold custom-primary-hover float-left mr-3">Зайти на канал</a>
                <a href="{{ url('/profile/channels/' ~ channel.getId()) ~ '/edit'}}" class="text-decoration-none text-uppercase text-color-dark font-weight-semibold custom-primary-hover float-left mr-3">Редактировать</a>

                <a href="{{ url('/profile/channels/' ~ channel.getId() ~ '/telegram') }}" class="text-decoration-none text-uppercase text-color-dark font-weight-semibold custom-primary-hover float-left">Натройка телеги</a>
            </div>
        </article>
        {% endfor %}
    </div>
</div>
<div class="row justify-content-around mb-5">
    <div class="col-md-12 ml-auto">
        <a href="{{ url('profile/channels/create') }}" class="btn btn-info btn-block">Создать новый канал</a>
    </div>
</div>
{% endblock %}

{% block styles %}
    <link rel="stylesheet" href="{{ url('css/animate.min.css') }}" />
    {{ super() }}
{% endblock %}

{% block scripts %}
    {{ super() }}
    <script type="text/javascript" src="{{ url('js/animatedModal.min.js') }}"></script>
{% endblock %}