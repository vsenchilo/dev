{% extends 'main.volt' %}

{% block content %}
<div class="row">
    <div class="col">
        <section class="card card-admin">
            <header class="card-header">
                <h2>Редактирование канала</h2>
            </header>
            <div class="card-body">
                <form class="form-horizontal form-bordered">
                    <h3>Основное</h3>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2" for="channel-title">Название
                            канала</label>
                        <div class="col-lg-6 position-relative">
                            <input type="text" class="form-control" id="channel-title"
                                value="" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2" for="channel-alias">Alias
                            канала</label>
                        <div class="col-lg-6 position-relative">
                            <input type="text" class="form-control" id="channel-alias"
                                value="" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2" for="channel-description">Описание
                            канала</label>
                        <div class="col-lg-6 position-relative">
                            <textarea class="form-control"
                                id="channel-description"></textarea>
                        </div>
                    </div>
                    <hr>
                    <h3>Meta данные<br><span class="small" title="Описание для мета тегов">Что это ?</span></h3>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2" for="channel-meta-description">Meta
                            description</label>
                        <div class="col-lg-6 position-relative">
                            <textarea class="form-control"
                                id="channel-meta-description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2" for="channel-meta-keywords">Meta
                            keywords</label>
                        <div class="col-lg-6 position-relative">
                            <textarea class="form-control"
                                id="channel-meta-keywords"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <footer class="card-footer">
                <button type="button" class="btn btn-primary float-right" id="create-channel">Сохранить</button>
            </footer>
        </section>
    </div>
</div>
{% endblock %}


{% block scripts %}
    {{ super() }}
    <script type="text/javascript" src="{{ url('js/channel/create.js') }}"></script>
{% endblock %}