{% extends 'main.volt' %}

{% block content %}
<div class="row justify-content-around">
    <div class="col-md-10 ml-auto">
        {% for channel in subscriptions %}
        <article class="custom-post-style-2 mb-5">
            <h3 class="text-color-dark font-weight-bold text-uppercase mb-2">{{ channel.getTitle() }}</h3>
            <p class="custom-text-color-1">{{ channel.getDescription() }}</p>
            <div class="post-links">
                {% if user %}
                    {% if user.getId() != channel.getOwner() %}
                        <a style="cursor: pointer;" class="text-decoration-none text-uppercase text-color-dark font-weight-semibold custom-primary-hover float-left" data-channel="{{ channel.getId() }}">Отписаться</a>
                    {% endif %}
                {% else %}
                    Что бы подписаться на канал, необходимо авторизоваться
                {% endif %}
            </div>
        </article>
        {% endfor %}
    </div>
</div>
{% endblock %}

{% block scripts %}
    {{ super() }}
{% endblock %}