{% extends 'main.volt' %}

{% block content %}
<div class="row">
    <div class="col">
        <section class="card card-admin">
            <header class="card-header">
                <h2>Настройки телеграм бота</h2>
            </header>
            <div class="card-body">
                <form class="form-horizontal form-bordered">
                    <div class="form-group row">
                        <label class="col-lg-3 control-label text-lg-right pt-2" for="telegram-token">Telegram token</label>
                        <div class="col-lg-6 position-relative">
                            <input type="text" class="form-control" id="telegram-token" value="{{ telegram != null ? telegram.getToken() : '' }}" />
                            <input type="hidden" class="form-control" id="channel-id" value="{{ channel.getId() }}" />
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <h2 class="text-color-dark font-weight-bold text-uppercase mb-3">Как настроить бота?</h2>
                        <ol>
                            <li>Открываем telegram (приложение или <a href="https://web.telegram.org" target="_blank">web версию</a>)</li>
                            <li>В поиске контактов ищем <code>@BotFather</code></li>
                            <li>Жмем кнопку <code>START</code></li>
                            <li>Вводим команду <code>/newbot</code> отправляем сообщение или жмем <code>enter</code><br>Далее бот нам предлагает придумать и ввести имя для своего бота.</li>
                            <li>Вводим <b>название</b> бота, например <code>your_channel</code> отправляем сообщение или жмем <code>enter</code></li>
                            <li>Далее нам предлагают выбрать <b>имя</b> для бота<br><b>Имя бота обязательно должно заканчиваться на bot</b>.<br>Вводим имя для бота, например <code>your_channel_bot</code> отправляем сообщение или жмем <code>enter</code></li>
                            <li>Затем нас поздравляют с тем, что мы все же смогли создать бота, и выдают нам наш персональный API ключ,<br>выглядит пример так <code>753777321:AAQPKzDvq6Rw2qMsgUx1MbPasUjq32zGDFs</code>.<br><b>API ключ это Ваш "паспорт" для бота, и не рекомендуется (крайне не рекомендуется) где либо его компрометировать</b></li>
                            <li>Осталось всего ничего, копируем ключ (<code>ctrl+c/ctrl+v</code>)<br>или вводим вручную (ведь трудности нам не страшны!) в <label for="telegram-token" class="btn-link">поле</label> и жмем на кнопку <label for="connect-telegram" class="btn-link">сохранить</label></li>
                            <li>Если Вы все сделали правильно, Ваши подписчики смогут получать Ваши уведомления от бота (естественно, они должны быть подписаны на вашего бота, но это не проблема, после добавление Вашего бота в нашу систему, посредством API ключа, ваш бот легко сможет рассказать вашим подписчикам, как быстро и легко им подписаться на Ваш канал)</li>
                        </ol>
                    </div>
                </form>
            </div>
            <footer class="card-footer">
                <button type="button" class="btn btn-primary float-right" id="connect-telegram">Сохранить</button>
            </footer>
        </section>
    </div>
</div>
{% endblock %}

{% block scripts %}
    {{ super() }}
    <script type="text/javascript" src="{{ url('js/channel/telegram.js') }}"></script>
{% endblock %}