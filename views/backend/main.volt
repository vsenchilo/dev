<!DOCTYPE html>
<html lang="{{ documentLang }}">
<head>
    {% block title %}{% endblock %}
    {% block styles %}{% endblock %}
</head>
<body>
{% block content %}{% endblock %}
<footer>
    {% block scripts %}{% endblock %}
</footer>
</body>
</html>