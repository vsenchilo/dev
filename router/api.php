<?php

declare (strict_types = 1);

$router->addPost('/api/v1/signup/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'auth',
    'action' => 'signup'
])->setName('apiSignup');

$router->addPost('/api/v1/signin/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'auth',
    'action' => 'signin'
])->setName('apiSignin');


$router->add('/api/v1/telegram/webhook/{channel}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'telegram',
    'action' => 'webhook',
]);

$router->addPost('/api/v1/channels/list/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'channels',
    'action' => 'list',
]);

$router->addPost('/api/v1/telegram/list/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'channels',
    'action' => 'telegramlist',
]);

$router->addPost('/api/v1/channel/create/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'channels',
    'action' => 'create',
]);

$router->addPost('/api/v1/channel/telegram/connect/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'telegram',
    'action' => 'connect',
]);

$router->addPost('/api/v1/channel/{id:[a-z0-9]{24}}/message/send/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'channels',
    'action' => 'sendmessage',
]);


// $router->addPost('/api/v1/email/confirm', [
//     'namespace' => 'App\\Http\\Api\\Controllers',
//     'controller' => 'auth',
//     'action' => 'emailConfirm'
// ])->setName('apiConfirmEmail');

//$router->addPost('/api/v1/channel/subscribe/{id:[a-z0-9]{24}}/a/{storage:(cookies|headers)}', [
$router->addPost('/api/v1/channel/subscribe/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'channels',
    'action' => 'subscribe',
])->setName('apiChannelSubscribe');


$router->addPost('/api/v1/test/a/{storage:(cookies|headers)}', [
    'namespace' => 'App\\Http\\Api\\Controllers',
    'controller' => 'test',
    'action' => 'test',
]);

// $router->add('/api/v1/password/reset/{email:(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$})', [
//     'namespace' => 'App\\Http\\Api\\Controllers',
//     'controller' => 'auth',
//     'action' => 'resetPassword'
// ])->setName('apiResetPassword');
