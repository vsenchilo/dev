<?php

$router->addGet('/', [
    'namespace'  => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'index',
    'action'     => 'index',
]);

$router->addGet('/{channel}/channel', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'index',
    'action' => 'channel',
]);

$router->addGet('/profile/channels', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'channels',
    'action' => 'list',
]);

$router->addGet('/profile/channels/{id:[a-z0-9]{24}}', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'channels',
    'action' => 'channel',
]);

$router->addGet('/profile/channels/{id:[a-z0-9]{24}}/edit', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'channels',
    'action' => 'edit',
]);

$router->addGet('/profile/channels/{id:[a-z0-9]{24}}/telegram', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'channels',
    'action' => 'telegram',
]);

$router->addGet('/profile/channels/create', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'channels',
    'action' => 'create',
]);

$router->addGet('/profile/channels/subscriptions', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'channels',
    'action' => 'subscriptions',
]);

$router->addGet('/profile/project/technical', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'index',
    'action' => 'technical',
]);

$router->addGet('/__/users/list', [
    'namespace' => 'App\\Http\\Frontend\\Controllers',
    'controller' => 'index',
    'action' => 'users',
]);
